from django.test import TestCase, Client, LiveServerTestCase
from django.contrib.auth.models import User
from django.http import HttpRequest
from django.urls import resolve

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

from faker import Faker

import os
import time
import random
import string


# Create your tests here.

class UnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 302)
    

    def test_create_User(self):
        fake = Faker()
        username = fake.name()
        email = fake.email()
        password = fake.pystr(min_chars = 8, max_chars=10)
        user = User.objects.create_user(username, email, password)

        counter = User.objects.all().count()
        self.assertEqual(counter, 1)

        data = User.objects.get(username=username)
        self.assertEqual(data.email, email)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super(FunctionalTest, self).setUp()

        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument("--headless")
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('disable-gpu')

        if 'GITLAB_CI' in os.environ:
            self.browser = webdriver.Chrome(
                './chromedriver', chrome_options=chrome_options)
        else:
            self.browser = webdriver.Chrome(chrome_options=chrome_options)        

    def tearDown(self):
        self.browser.quit()

        super(FunctionalTest, self).tearDown()
    """
    def test_login_logout(self):
        User.objects.create_user('randomname', 'temp@mail.com', 'randompass')

        self.browser.get(self.live_server_url + '/')
 
        time.sleep(5) 

        username = self.browser.find_element_by_xpath('//input[@name="username"]')
        username.send_keys("randomname")

        password = self.browser.find_element_by_xpath('//input[@name="password"]')
        password.send_keys("randompass")

        login_button = self.browser.find_element_by_id("logInButton")

        time.sleep(10)
        self.assertIn('randomname', self.browser.page_source)

        logout_button = self.browser.find_element_by_id('logOutBtn')
        logout_button.click()
        time.sleep(10)
        self.assertNotIn('randomname', self.browser.page_source)
   """


