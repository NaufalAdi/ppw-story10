$(document).ready(function(){
    $('#createAccountLink').on('click', function(){
        $('#logIn').addClass('hide');
        $('#signUp').removeClass('hide');

        $('.error-message').css('display', 'none');
        $('.success-message').css('display', 'none');
    })
    
    $('#loginLink').on('click', function(){
        $('#logIn').removeClass('hide');
        $('#signUp').addClass('hide');

        $('.error-message').css('display', 'none');
        $('.success-message').css('display', 'none');
    })
    
    $('#logInButton').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '/api/v1/login/',
            data: JSON.stringify({
                'username' : $('input[name="username"]').val(),
                'password' : $('input[name="password"]').val()
            }),
            contentType: 'application/json',
            success: function(data, textStatus, jqXHR){
                if (data.status == 200){
                    window.location.replace("/");
                }else{
                    $('.error-message').html(data.message);
                    $('.error-message').css('display', 'inline-block');
                }
            }
        })
    })

    $('#signUpButton').on('click', function(){
        $.ajax({
            type: 'POST',
            url: '/api/v1/signup/',
            data: JSON.stringify({
                'username' : $('input[name="sign_username"]').val(),
                'email': $('input[name="sign_email"]').val(),
                'password' : $('input[name="sign_password"]').val()
            }),
            contentType: 'application/json',
            beforeSend: function(jqXHR, settings){
                $('.error-message').css('display', 'none');
                $('.success-message').css('display', 'none');
            },
            success: function(data, textStatus, jqXHR){
                if (data.status == 200){
                    $('.success-message').html(data.message);
                    $('.success-message').css('display', 'inline-block');

                    $('#logIn').removeClass('hide');
                    $('#signUp').addClass('hide');
                }else{
                    $('.error-message').html(data.message);
                    $('.error-message').css('display', 'inline-block');
                }
            }
        })
    });

    $('input').keypress(function (e) {
        if (e.which == 13) {
            if($('#logIn').hasClass('hide')){
                $('#signUpButton').click()
            }else if ($('#signUp').hasClass('hide')){
                $('#logInButton').click()
            }
            return false;    
        }
    });
})    
